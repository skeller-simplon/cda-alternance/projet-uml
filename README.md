## PROJET UML ##



Ce projet est un exercice réaliser pour la formation CDA Simplon. Le but étant de se perfectionner dans l'apprentissage du maquetage applicatif.



Ce projet contient :

- Un diagramme d'entités

  <img src="./screenshots/class-diagram.png" style="zoom:80%;" />

- Un diagramme de classes représentant l'architecture tri-couches du back-office

  <img src="./screenshots/Class diagram MVC.png">

- Un diagramme de use-case ainsi que plusieurs use-case écrits

  <img src="./screenshots/usecase-diagram.png"  />

- Trois diagrammes d'activité (nb: ces diagrammes représentent des philosophies de conception variées, dans un développement applicatif réel, il conviendrait de les harmoniser)
  - Manage promos
  - Manage projets
  - Manage assignement

<img src="./screenshots/activity-diagram-example.png" style="zoom:50%;" />

- Deux diagrammes de séquence

  <img src="./screenshots/Sequence-diagram.jpg" style="zoom:50%;" />

- Des maquêtes fonctionnelles de l'application

| <img src="./mockup/Projet-UML Mockups-Login Page.jpg"  />    | <img src="./mockup/Projet-UML Mockups-Accueil.jpg" alt="Projet-UML Mockups-Accueil"  /> |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="./mockup/Projet-UML Mockups-Page project.jpg" alt="Projet-UML Mockups-Page project"  /> | <img src="./mockup/Projet-UML Mockups-Admin.jpg" style="zoom:;" /> |

- Un README

- Une liste de fonctionnalités réparties en sprint. Les sprints sont de deux semaines. Les poids ont été calculés avec un [Planning Poker](https://fr.wikipedia.org/wiki/Planning_poker) et sont repartis de manière équilibrés.
  
  - Les différentes milestones sont trouvables sur ce [gitlab](https://gitlab.com/skeller-simplon/cda-alternance/projet-uml/-/milestones)
  
    





> # Consigne
>
> ## Contexte
>
> Un réseau de centre de formation (qu'on ne nommera pas) souhaite créer une plateforme unifié pour regrouper 
> les différentes tâches courantes propres à chaque centre.
>
> Les besoins sont les suivants : 
> * Les directeurices de régions doivent pouvoir gérer les différents centres de formation
> * Les chargé·e·s de promos doivent pouvoir gérer les promotions du ou des centre(s) auxquels ielles sont rattaché·e·s
>     * Créer des nouvelles promotions
>     * Inviter des apprenant·e·s dans les promos
>     * Valider une candidature
>     * Assigner un·e formateur·ice à une promotion
>     * Gérer les retards et absences des apprenant·e·s
> * Les formateur·ice·s doivent pouvoir créer des projets puis les assigner à leurs promos ainsi que les corriger
> * On doit être capable de consulter l'agenda des formations en cours ou à venir
> * On doit pouvoir candidater à une formation à venir
> * Un·e apprenant·e doit avoir accès aux projets proposés à sa promotion ainsi que proposer un rendu. Ielle doit également pouvoir consulter son avancement dans les compétences
>
> Les promos sont liées à un référentiel (DWWM, CDA, TAI...) qui détermine les compétences et les projets assignables à cette promotion. Il est probable que d'autres référentiels soient rajoutés par la suite.
>
> D'autres fonctionnalités peuvent être envisagées : Gestion des livrets d'évaluation, dossier professionnels et dossier projets via la plateforme. Gestion de l'agenda des promotions.
>
> ## Objectifs
> En groupe de 3-4, vous devrez créer une ébauche de spécifications fonctionnelles et techniques pour ce projet.
> Il faudra créer les différents diagramme UML (use case, activité, classes, séquence, état-transition), faire quelques maquettes fonctionnelles.
>
> Vous pourrez ensuite définir les sprints et les tâches (via gitlab) ainsi que définir le poids de celles ci via un planning poker.